package latihan.fajarcahyoprabowo.wifibandungjuara

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import latihan.fajarcahyoprabowo.wifibandungjuara.adapter.LokasiAdapter
import latihan.fajarcahyoprabowo.wifibandungjuara.rest.WifiApiService


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbarSetting()

        val layout = LinearLayoutManager(this)
        rv_lokasi_wifi.layoutManager = layout

        loadData()

        refreshLokasi.setOnRefreshListener {
            loadData()
        }

    }

    // load data lokasi wifi
    fun loadData(){
        refreshLokasi.isRefreshing = true
        val apiService = WifiApiService.create()
        val call = apiService.getLokasi()
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    hasil ->
                    if(hasil.lokasi.size.equals(null)){
                        tv_ket.setVisibility(View.VISIBLE)
                        refreshLokasi.isRefreshing = false
                    }else{
                        tv_ket.setVisibility(View.GONE)
                        val adapter = LokasiAdapter(hasil.lokasi,this)
                        adapter.notifyDataSetChanged()
                        rv_lokasi_wifi.adapter = adapter
                    }
                },{
                    error ->
                    error.printStackTrace()
                    refreshLokasi.isRefreshing = false
                },{
                    refreshLokasi.isRefreshing = false
                })
    }


    // untuk setting toolbar
    fun toolbarSetting(){
        supportActionBar!!.title = " WIFI BANDUNG JUARA"
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setIcon(R.mipmap.ic_luncher_app)
    }
}
