package latihan.fajarcahyoprabowo.wifibandungjuara

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_detail.*
import latihan.fajarcahyoprabowo.wifibandungjuara.adapter.DetailLokasiAdapter
import latihan.fajarcahyoprabowo.wifibandungjuara.model.Lokasi
import latihan.fajarcahyoprabowo.wifibandungjuara.rest.WifiApiService

class DetailActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    var lat : Double? = null
    var long : Double? = null

    var googleApiClient : GoogleApiClient? = null
    var location : Location? = null

    var REQUEST_LOCATION : Int = 100


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setupGoogleApi()

        val layout = LinearLayoutManager(this)
        rv_detail_lokasi.layoutManager = layout

        val baseUrl = "http://latihanwebservice.comeze.com/"

        // mendapatkan data lokasi wifi dari activity yang memanggil
        val data = intent.extras["data"] as Lokasi

        // setting toolbar
        supportActionBar?.title = "WIFI BANDUNG JUARA"
        supportActionBar?.subtitle = data.nama_lokasi
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tv_nama_lokasi.text = data.nama_lokasi

        // menampilkan gambar menggunakan picasso
        Picasso.with(this).load(baseUrl+data.foto).fit().into(img_lokasi)


        tv_judul_list.text = "Titik Wifi : "+data.jumlah
        loadData(data.kode_lokasi)

    }

    // mendapatkan data wifi per lokasi
    fun loadData(id : String) {

        val apiService = WifiApiService.create()
        val call = apiService.getDetailLokasi(id)
        call.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    hasil ->
                        val map = mapOf(
                                "latitude" to lat,
                                "longitude" to long
                        )

                        val adapter = DetailLokasiAdapter(hasil.detail_lokasi,this,map)
                        adapter.notifyDataSetChanged()
                        rv_detail_lokasi.adapter = adapter
                },{
                    error ->
                        error.printStackTrace()
                },{

                })
    }


    // setup googleApi
    fun setupGoogleApi(){
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 500
        locationRequest.fastestInterval = 100

        googleApiClient = GoogleApiClient
                .Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }


    fun getLokasi() {

        val izin = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)


        // cek izin mendapatkan lokasi
        if(izin==PackageManager.PERMISSION_GRANTED) {

            // mendapatkan lokasi
            location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient)
            if(location!=null){
                lat = location?.latitude
                long = location?.longitude
            }else{
                Toast.makeText(this, " Gagal Mendapatkan Lokasi", Toast.LENGTH_LONG).show()
            }
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            REQUEST_LOCATION -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLokasi()
                }else if(grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_LOCATION)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onConnected(p0: Bundle?) {
        getLokasi()
    }

    override fun onConnectionSuspended(p0: Int) {
        Toast.makeText(this," Tidak ada koneksi", Toast.LENGTH_LONG).show()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Toast.makeText(this, " Tidak bisa tehubung dengan GPS", Toast.LENGTH_LONG).show()
    }

    override fun onStart() {
        super.onStart()
        googleApiClient?.connect()
    }

    override fun onStop() {
        super.onStop()
        googleApiClient?.disconnect()
    }


}
