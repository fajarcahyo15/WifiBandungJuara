package latihan.fajarcahyoprabowo.wifibandungjuara.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by fajar on 31/07/17.
 */

open class Lokasi(

        val no : Int,
        val nama : String,
        val latitude : String,
        val longitude : String,
        val fasilitas : String,
        val status : String,
        val kode_lokasi : String,
        val nama_lokasi : String,
        val foto : String,
        val kode_kecamatan : String,
        val nama_kecamatan : String,
        val jumlah : Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(no)
        parcel.writeString(nama)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(fasilitas)
        parcel.writeString(status)
        parcel.writeString(kode_lokasi)
        parcel.writeString(nama_lokasi)
        parcel.writeString(foto)
        parcel.writeString(kode_kecamatan)
        parcel.writeString(nama_kecamatan)
        parcel.writeInt(jumlah)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Lokasi> {
        override fun createFromParcel(parcel: Parcel): Lokasi {
            return Lokasi(parcel)
        }

        override fun newArray(size: Int): Array<Lokasi?> {
            return arrayOfNulls(size)
        }
    }
}


