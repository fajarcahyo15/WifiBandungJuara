package latihan.fajarcahyoprabowo.wifibandungjuara.model

/**
 * Created by fajar on 31/07/17.
 */
data class Hasil(
        val status: Int,
        val message: String,
        val lokasi : List<Lokasi>,
        val detail_lokasi : List<DetailLokasi>
)