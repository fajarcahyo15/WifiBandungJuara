package latihan.fajarcahyoprabowo.wifibandungjuara.rest

import io.reactivex.Observable
import latihan.fajarcahyoprabowo.wifibandungjuara.model.Hasil
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by fajar on 31/07/17.
 */
interface WifiApiService {

    // mendapatkan data lokasi wifi menggunakan method GET
    @GET("api/lokasi")
    fun getLokasi(): Observable<Hasil>

    // mendapatkan data wifi per lokasi dengan mehod get
    @GET("api/detail_lokasi/{id}")
    fun getDetailLokasi(@Path("id") id:String): Observable<Hasil>

    //setting Retrofit menggunakan RxJava 2 dengan baseUrl http://latihanwebservice.comeze.com/
    companion object Factory {
        fun create(): WifiApiService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://latihanwebservice.comeze.com/")
                    .build()

            return retrofit.create(WifiApiService::class.java);
        }
    }
}