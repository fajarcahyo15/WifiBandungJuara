package latihan.fajarcahyoprabowo.wifibandungjuara.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.lokasi_list.view.*
import latihan.fajarcahyoprabowo.wifibandungjuara.DetailActivity
import latihan.fajarcahyoprabowo.wifibandungjuara.R
import latihan.fajarcahyoprabowo.wifibandungjuara.model.Lokasi


/**
 * Created by fajar on 31/07/17.
 */
class LokasiAdapter(val wifiList : List<Lokasi>, val ctx : Context) : RecyclerView.Adapter<LokasiAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): LokasiAdapter.ViewHolder{
        val v =  LayoutInflater.from(parent!!.context).inflate(R.layout.lokasi_list,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: LokasiAdapter.ViewHolder, position: Int) {

        val data = wifiList.get(position)
        val baseUrl = "http://latihanwebservice.comeze.com/"

        holder?.tv_lokasi?.text = data.nama_lokasi
        holder?.tv_status?.text = "Jumlah Wifi Aktif : "+data.jumlah.toString()
        Picasso.with(ctx).load(baseUrl+data.foto).fit().into(holder.icon)

        holder?.itemView.setOnClickListener {
            val intent  = Intent(ctx,DetailActivity::class.java)
            intent.putExtra("data",wifiList.get(position))
            ctx.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return wifiList.size
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val tv_lokasi = itemView.tv_lokasi
        val tv_status = itemView.tv_status
        val icon = itemView.icon
    }


}

