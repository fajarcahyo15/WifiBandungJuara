package latihan.fajarcahyoprabowo.wifibandungjuara.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.detail_lokasi_list.view.*
import latihan.fajarcahyoprabowo.wifibandungjuara.R
import latihan.fajarcahyoprabowo.wifibandungjuara.model.DetailLokasi

/**
 * Created by fajar on 31/07/17.
 */
class DetailLokasiAdapter(val wifiList: List<DetailLokasi>,
                          val ctx: Context,
                          val map: Map<String, Double?>
) : RecyclerView.Adapter<DetailLokasiAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int):DetailLokasiAdapter.ViewHolder {
        val v =  LayoutInflater.from(parent!!.context).inflate(R.layout.detail_lokasi_list,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: DetailLokasiAdapter.ViewHolder?, position: Int) {

        val data = wifiList.get(position)

        holder?.tv_nama?.text = data.nama.toUpperCase()
        holder?.tv_status?.text = data.status

        val lat = map["latitude"]
        val long = map["longitude"]

        holder?.itemView?.setOnClickListener {
            val uri = "http://maps.google.com/maps?saddr=${lat.toString()},${long.toString()}&daddr=${data.latitude},${data.longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent.setPackage("com.google.android.apps.maps")
            ctx.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return wifiList.size
    }



    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tv_nama = itemView.tv_nama
        val tv_status = itemView.tv_status
    }

}